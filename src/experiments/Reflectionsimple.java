package experiments;


public class Reflectionsimple {
	public double num1 = 55.5;
	private double num2 = 86.6;
	
	
	public Reflectionsimple() {
		
	}
	public Reflectionsimple (double num1,  double num2) {
		this.num1=num1;
		this.num2=num2;
	}
	public void squrenum1() {
		this.num1 =Math.sqrt(this.num1);		
	}
	public void squrenum2() {
		this.num2 =Math.sqrt(this.num2);	
	}
	public double getnum1() {
		return num1;
	}
	public double getnum2() {
		return num2;
	}
	public void  setnum1(double num1) {
		this.num1 *=this.num1;
	}
	public void  setnum2(double num2) {
		this.num2 *=this.num2;
	}
	public String toString() {
		return String.format("num1 : %.2f\nnum2 : %.2f", num1, num2);
		}
}

