package experiments;

import java.lang.reflect.Field;

public class Reflection05 {
    public static void main(String[] args) throws Exception {
        Reflectionsimple s = new Reflectionsimple();

        System.out.println("Reflection 05");
        Field[] fields = s.getClass().getDeclaredFields();
        System.out.printf("There are %d fields\n", fields.length);

        for (Field f : fields) {
            // Set the accessible flag to true to access private fields
            f.setAccessible(true);
            System.out.printf("field name=%s type=%s value=%.2f\n", f.getName(),
                    f.getType(), f.getDouble(s));
        }
    }
}
