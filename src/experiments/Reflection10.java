package experiments;

import java.lang.reflect.*;

public class Reflection10 {
  public static void main(String[] args) throws Exception {
	  Reflectionsimple s = new Reflectionsimple();
	  System.out.println("Reflection 10");
    Method m = s.getClass().getDeclaredMethod("setnum1", double.class);
    m.setAccessible(true);
    m.invoke(s, 76);
    System.out.println(s);
  }
}
