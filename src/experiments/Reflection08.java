package experiments;

import java.lang.reflect.*;

public class Reflection08 {
  public static void main(String[] args) throws Exception {
	  Reflectionsimple s = new Reflectionsimple();
	  System.out.println ("Reflection 08");
    Field[] fields = s.getClass().getDeclaredFields();
    System.out.printf("There are %d fields\n", fields.length);
    for (Field f : fields) {
      f.setAccessible(true);
      Double num=f.getDouble(s);
       num++;
      f.setDouble(s, num);
      System.out.printf("field name=%s type=%s value=%.2f\n", f.getName(),  
          f.getType(),f.getDouble(s));
    }
  }
}
