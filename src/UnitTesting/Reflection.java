package UnitTesting;

public class Reflection {
	
	public double num1 = 50.5;
	private double num2 = 84.2;
	
	public Reflection() {	
	}
	public Reflection(double num1, double num2) {
		this.num1 = num1;
		this.num2 = num2;
	}
	public void squarenum1() {
		this.num1 = Math.sqrt(this.num1);
	}	
	public double getnum1() {
		return num1;
	}	
	public double getnum2() {
		return num2;
	}	
	public void setnum2(double num2) {
		this.num2 = num2;
	}	
	public String toString() {
		return String.format("\nnum1 : %.2f\nnum2 : %.2f " , num1, num2);
	}
}

