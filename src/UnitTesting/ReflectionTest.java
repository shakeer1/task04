package UnitTesting;
import static kunit1.Kunit.*;

public class ReflectionTest {

  void Reflectiontest1(){
	  
	  Reflection reflection = new Reflection(50.5, 84.2);
	  test1(reflection.getnum1(), 50.5);
	  test1(reflection.getnum2(), 84.2);
	  test2(reflection.getnum2(), 84.2);    
	  test2(reflection.getnum2(), 50.5);    
  }

	void Reflectiontest2(){
		
		Reflection reflection = new Reflection(50.5, 84.2);
		reflection.squarenum1();
	    test1(reflection.getnum1(), 50.5);
	}
	

void Reflectiontest3(){
		Reflection reflection = new Reflection(50.5, 84.2);
		  test4("2011549", "1234");
		  reflection = new Reflection(50.5, 0);
		  test4("Shajith", "Shakeer");
		  reflection = new Reflection(50.5, 0);
		  test4("N", "Y");
		  
	}

  public static void main(String[] args) {
	  
	  ReflectionTest test = new ReflectionTest();
	  test.Reflectiontest1();
	  test.Reflectiontest2();
	  test.Reflectiontest3();
      report();
  }
}


